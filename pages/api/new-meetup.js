import { MongoClient } from "mongodb";
// must have handler function to be the api endpoint
// POST /api/new-meetup

async function handler (req, res) {
    if (req.method === 'POST') {
        const data = req.body;

        const client = await MongoClient.connect('mongodb+srv://hoda:1234@clustermeetup.oqkastp.mongodb.net/?retryWrites=true&w=majority&appName=ClusterMeetup');
        const db = client.db();
        console.log("client db", db)
        const meetupsCollection = db.collection('meetups');
        const result = await meetupsCollection.insertOne(data);
        console.log(result);

        client.close();
        res.status(201).json({ message : 'meetup inserted!'});
    
    }
}
export default handler;