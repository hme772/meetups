import NewMeetupForm from "../../components/meetups/NewMeetupForm";
import Head from "next/head";
import { Fragment } from "react";

const NewMeetupPage = () => {
  async function onAddMeetup(enteredMeetupData) {
    console.log(enteredMeetupData);
    const response = await fetch("/api/new-meetup", {
      method: "POST",
      body: JSON.stringify(enteredMeetupData),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    console.log("NewMeetupPage", data);
  }

  return (
    <Fragment>
      <Head>
        <title>Add A New Meetup</title>
        <meta
          name="description"
          content="Add your own meetup!"
        />
      </Head>
      <NewMeetupForm onAddMeetup={onAddMeetup} />
    </Fragment>
  );
};

export default NewMeetupPage;
