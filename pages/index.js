import { MongoClient } from "mongodb";
import MeetupList from "../components/meetups/MeetupList";
import Head from 'next/head';
import { Fragment } from "react";

const HomePage = (props) => {
  return <Fragment>
    <Head>
      <title>React NextJs Meetups</title>
      <meta name='description' content='Browse a huge list of highly active React NextJs meetups!'/>
    </Head>

    <MeetupList meetups={props.meetups} />
  </Fragment>;
};
export async function getStaticProps () {

  const client = await MongoClient.connect('mongodb+srv://hoda:1234@clustermeetup.oqkastp.mongodb.net/?retryWrites=true&w=majority&appName=ClusterMeetup');
  const db = client.db();
  const meetupsCollection = db.collection('meetups');
  const meetups = await meetupsCollection.find().toArray();

  client.close(); 

  return {
    props: {
      meetups: meetups.map(meetup => {
        const {_id, ...relevantData} = meetup;
        return {...relevantData, id: meetup._id.toString()};
      })
    },
    revalidate: 10
  };
}

export default HomePage;
