import { MongoClient, ObjectId } from "mongodb";
import { Fragment } from "react";
import MeetupDetails from "../../components/meetups/MeetupDetail";
import Head from "next/head";

const MeetupDetailsPage = (props) => {
  return (
    <Fragment>
    <Head>
      <title>{props.meetupData.title}</title>
      <meta name='description' content={props.meetupData.description}/>
    </Head>
    <MeetupDetails
      image={props.meetupData.image}
      description={props.meetupData.description}
      title={props.meetupData.title}
      address={props.meetupData.address}
    />
    </Fragment>  );
};

export async function getStaticPaths() {


    const client = await MongoClient.connect('mongodb+srv://hoda:1234@clustermeetup.oqkastp.mongodb.net/?retryWrites=true&w=majority&appName=ClusterMeetup');
    const db = client.db();
    const meetupsCollection = db.collection('meetups');
    const meetups = await meetupsCollection.find({},{_id:1}).toArray();
    client.close();

  return {
    fallback: "blocking",
    paths: meetups.map(meetup => ({params:{meetupId: meetup._id.toString()}})),
  };
}

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId;
  const client = await MongoClient.connect('mongodb+srv://hoda:1234@clustermeetup.oqkastp.mongodb.net/?retryWrites=true&w=majority&appName=ClusterMeetup');
  const db = client.db();
  const meetupsCollection = db.collection('meetups');

  const selectedMeetup = await meetupsCollection.findOne({_id: ObjectId(meetupId)});
  client.close();
  const {_id, ...relevantData} = selectedMeetup;

  return {
    props: {
      meetupData: {...relevantData, id: _id.toString()},
      revalidate: 10,
    },
  };
}

export default MeetupDetailsPage;
